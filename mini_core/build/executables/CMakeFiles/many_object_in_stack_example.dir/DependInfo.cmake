# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kitsune/Desktop/robotprogramming_2021-22/mini_core/executables/many_object_in_stack_example.cpp" "/home/kitsune/Desktop/robotprogramming_2021-22/mini_core/build/executables/CMakeFiles/many_object_in_stack_example.dir/many_object_in_stack_example.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/kitsune/Desktop/robotprogramming_2021-22/mini_core/build/src/CMakeFiles/core_library.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
