cmake_minimum_required(VERSION 2.8.3)
project(exercise_2)

# invokes the routines to find a package called Eigen3
find_path(EIGEN3_INCLUDE_DIR signature_of_eigen3_matrix_library
  /usr/include/eigen3
  /usr/local/include/eigen3
)

if( NOT EIGEN3_INCLUDE_DIR )
     message( FATAL_ERROR "Please point the environment variable EIGEN3_INCLUDE_DIR to the include directory of your Eigen3 installation!")
endif()


# find Eigen3
find_package(Eigen3 REQUIRED)


# specify additional locations of header files
include_directories(
  ${EIGEN3_INCLUDE_DIR}
  ${PROJECT_SOURCE_DIR}/src
)


# add project source code
add_subdirectory(src)
# TODO do the same for executables
